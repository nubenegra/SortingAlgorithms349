import java.util.Arrays;

public class CountingSort
{
    public static int[] countingSort(int [] A)
    {
        //min and max values from array
        //this will help us determine the size of T dictionary
        //Also used javas stream packages
        int min = Arrays.stream(A).min().orElse(0);
        int max = Arrays.stream(A).max().orElse(Integer.MAX_VALUE);
        int[] T = new int[max - min + 1];//dictionary
        int [] B = new int[A.length];//this will store sorted array
        int currentVal, pos;
       //find the occurrences of the elements in A and increment them in the corresponding index of T
        for (int current: A)
            T[current - min]++;

        //now we are adding the elements from left to right cumulatively
        for(int index = 1; index < T.length; index++ )
            T[index] += T[index - 1];

        //now we are storing the values into the B sequence
        for(int subscript = A.length - 1; subscript >= 0; subscript--)
        {
            currentVal = A[subscript];
            pos = T[currentVal - min] - 1;
            B[pos] = currentVal;
            T[currentVal - min]--;
        }
        return B;
    }

//
//    public static void main(String[] args)
//    {
//        long startTime = System.currentTimeMillis();
//        int[] array = {2, -1, 9, 8, 5, -3, 0, 8};
//
//        String string = Arrays.toString(countingSort(array)).replace('[', ' ').
//                replace(']', ' ').trim();
//
//        System.out.println(string);
//
//        System.out.println("RunTime");
//        long endTime = System.currentTimeMillis();
//        System.out.println(endTime - startTime);
//    }
}
