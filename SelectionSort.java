import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Arrays;

public class SelectionSort
{
    public static ArrayList<Integer> selectionSort (int [] array)
    {
        //made into list so it's easier to remove
        List<Integer> A = Arrays.stream(array).boxed().collect(Collectors.toList());
        //used wrapper class to remove objects from list rather than remove from index with primitive
        Integer min;
        ArrayList<Integer> B = new ArrayList<>();
        
        while(!A.isEmpty())
        {
            min = Collections.min(A);
            B.add(min);
            A.remove(min);
        }
        return B;
    }

//    public static void main(String[] args)
//    {
//        long startTime = System.currentTimeMillis();
//        int[] array = {2, -1, 9, 8, 5, -3, 0, 8};
//        ArrayList<Integer> A = loadList(array);
//        ArrayList<Integer> B = selectionSort(A);
//
//        String string = B.toString().replace('[', ' ').replace(']', ' ').trim();
//
//        System.out.println(string);
//        System.out.println("RunTime");
//
//        long endTime = System.currentTimeMillis();
//        System.out.println(endTime - startTime);
//    }
}
